import { View, Text, Alert, FlatList } from 'react-native'
import React, { useState, useEffect} from 'react'

export default function TodoScreen() {
  const [ todos, setTodos ] = useState([]);

  const fetchTodos = async () => {
    try {
        let response = await fetch('https://jsonplaceholder.typicode.com/todos');
        if (response.ok) {
            let data = await response.json();
            setTodos(data);
        } else {
            throw new Error('Something went wrong');
        }
    } catch(err) {
        Alert.alert('Something went wrong')
    }
  }

  useEffect(() => {
    fetchTodos();
  }, []);

  return (
    <View>
      <Text>TodoScreen</Text>
      <FlatList
        data={todos}
        renderItem={({item}) => (
            <View>
                <Text>{item.id}. {item.title}</Text>
            </View>
        )} 
      />
    </View>
  )
}