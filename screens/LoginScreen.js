import { View, Text, TextInput, TouchableOpacity, Alert } from 'react-native'
import React, { useEffect, useState } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function LoginScreen({ navigation }) {
  const [ username, setUsername ] = useState('');
  const [ password, setPassword ] = useState('');


  const getToken = async () => {
    try {
        const value = await AsyncStorage.getItem('token')
        if (value !== null) {
            navigation.navigate('Home')
        }
      } catch(e) {
        // error reading value
      }
  }

  useEffect(() => {
    getToken()
  }, [])

  const doLogin = async () => {
    try {
        // UNTUK REACT NATIVE ITU TIDAK MENGENAL localhost harus menggunakan IP
        let response = await fetch('http://192.168.100.238:8080/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: username,
                password: password
            })
        })

        if (response.ok) {
            let jwt = await response.json();
            // token kita taro di AsyncStorage
            await AsyncStorage.setItem('token', jwt.jwtToken)
            navigation.navigate('Home');
        } else {
            console.log('error')
        }

       
    } catch(err) {
        Alert.alert(err)
    }
    
  }

  return (
    <SafeAreaView>
      <Text>LoginScreen</Text>
      <TextInput onChangeText={setUsername} value={username} placeholder='Username'/>
      <TextInput onChangeText={setPassword} value={password} placeholder='Password'/>
      <TouchableOpacity onPress={doLogin}>
        <Text>Login</Text>
      </TouchableOpacity>
    </SafeAreaView>
  )
}