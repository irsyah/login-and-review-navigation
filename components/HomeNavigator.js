import { View, Text, Alert, TouchableOpacity } from 'react-native'
import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialIcons } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';

import TodoScreen from '../screens/TodoScreen';
import ProfileScreen from '../screens/ProfileScreen';
import LogoutScreen from '../screens/LogoutScreen';

const Tab = createBottomTabNavigator();

export default function HomeNavigator({ navigation }) {

  const alertSignOut = () => (
    Alert.alert('Confirmation required', 'Do you really want to logout?',
        [
            { text: 'Accept', onPress: () => signout() },
            { text: 'Cancel'}
        ]
    )
  )

  const signout = async () => {
    try {
        console.log('masuk signout')
        await AsyncStorage.removeItem('token');
        navigation.navigate('Login')
      } catch(e) {
        // remove error
      }
    
  }

  return (
    <Tab.Navigator>
        <Tab.Screen name='Todo' component={TodoScreen}/>
        <Tab.Screen name='Profile' component={ProfileScreen}/>
        <Tab.Screen 
            name='Logout' 
            component={LogoutScreen}
            options={({ navigation }) => ({
                tabBarIcon: ((size, color) => (
                    <TouchableOpacity onPress={alertSignOut}>
                        <MaterialIcons name="logout" size={24} color="black" />
                    </TouchableOpacity>
                ))
            })}
        />
    </Tab.Navigator>
  )
}