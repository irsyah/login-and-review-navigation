import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreen from './screens/LoginScreen';
import HomeNavigator from './components/HomeNavigator';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useEffect, useState } from 'react';


const Stack = createNativeStackNavigator();

export default function App() {

  // const [ isLogin, setIsLogin ] = useState('');
  // const [ routeName, setRouteName ] = useState('Login');

  // const getToken = async () => {
  //   try {
  //       const value = await AsyncStorage.getItem('token')
  //       if (value !== null) {
  //         setIsLogin(value);
  //         setRouteName('Home');
  //       }
  //     } catch(e) {
  //       // error reading value
  //     }
  // }

  // useEffect(() => {
  //   getToken();
  //   console.log(routeName)
  // }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name='Login' component={LoginScreen}/>
        <Stack.Screen name='Home' component={HomeNavigator} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
